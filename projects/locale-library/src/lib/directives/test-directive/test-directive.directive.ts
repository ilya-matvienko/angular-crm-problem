import { Directive } from '@angular/core';

import { UserQuery } from 'locale-library/src/lib/module-with-store';

@Directive({ selector: '[libTestDirective]' })
export class TestDirective {
  constructor(
    private user: UserQuery,
  ) {
    console.warn(UserQuery);
  }
}
