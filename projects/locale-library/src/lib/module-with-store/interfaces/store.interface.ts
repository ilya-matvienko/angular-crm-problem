import { EntityState } from '@datorama/akita';
import { UserState } from './user.interface';

export interface UserEntityState extends EntityState<UserState, string> { }
