import { Injectable } from '@angular/core';

import { QueryEntity } from '@datorama/akita';

import { Observable } from 'rxjs';

import { UserEntityState } from '../interfaces';
import { UserStore } from './user.store';

@Injectable({ providedIn: 'root' })
export class UserQuery extends QueryEntity<UserEntityState> {
  name$: Observable<string> = this.select(state => state.name);

  constructor(protected store: UserStore) {
    super(store);
  }
}
