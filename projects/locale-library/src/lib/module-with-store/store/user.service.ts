import { Injectable } from '@angular/core';

import { UserStore } from './user.store';

@Injectable({providedIn: 'root' })
export class UserStateService {
  constructor(private store: UserStore) { }

  setName(name: string) {
    this.store.update({ name });
  }
}
