import { Injectable } from '@angular/core';
import { EntityStore, StoreConfig } from '@datorama/akita';

import { UserEntityState } from '../interfaces';

import { initialState } from './user.state';

@Injectable({ providedIn: 'root' })
@StoreConfig({  name: 'user'})
export class UserStore extends EntityStore<UserEntityState> {
  constructor() {
    super(initialState);
  }
}
