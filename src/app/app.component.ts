import { Component } from '@angular/core';

import { UserQuery } from 'locale-library';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-libs-problem';

  constructor(
    private user: UserQuery
  ) {
    console.warn(UserQuery);
  }
}
