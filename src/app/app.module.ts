import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DirectivesModule } from 'locale-library';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LazyModule } from './lazy-module/lazy.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DirectivesModule,

    // LazyModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
