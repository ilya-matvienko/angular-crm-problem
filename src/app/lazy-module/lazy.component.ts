import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { LazyFacade } from './lazy.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-lazy-module',
  templateUrl: './lazy.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LazyComponent implements OnInit {
  name$: Observable<string> = this.facade.name$;

  constructor(
    private facade: LazyFacade,
  ) { }

  ngOnInit(): void {
    this.facade.updateUser();

    this.name$.subscribe(i => console.log('LAZY COMPONENT', i));
  }
}
