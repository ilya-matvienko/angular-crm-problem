import { Injectable } from '@angular/core';

import { UserQuery, UserStateService } from 'locale-library';
import { Observable } from 'rxjs';

@Injectable()
export class LazyFacade {
  name$: Observable<string> = this.user.name$;

  constructor(
    private user: UserQuery,
    private userService: UserStateService,
  ) {
    console.warn('LAZY MODULE', UserQuery);
  }

  updateUser(): void {
    this.userService.setName('HUI');
  }
}
