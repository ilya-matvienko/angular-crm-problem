import { NgModule } from '@angular/core';

import { DirectivesModule } from 'locale-library';

import { LazyComponent } from './lazy.component';
import { LazyRouting } from './lazy.routing';
import { CommonModule } from '@angular/common';
import { LazyFacade } from './lazy.facade';

@NgModule({
  declarations: [
    LazyComponent,
  ],
  imports: [
    CommonModule,
    LazyRouting,
    DirectivesModule,
  ],
  providers: [
    LazyFacade,
  ],
})
export class LazyModule { }
